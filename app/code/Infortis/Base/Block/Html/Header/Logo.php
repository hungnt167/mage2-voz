<?php

namespace Infortis\Base\Block\Html\Header;

use Magento\Theme\Block\Html\Header\Logo as MagentoHeaderLogo;
use Magento\Framework\View\Element\Template\Context;
use Magento\MediaStorage\Helper\File\Storage\Database;
use Magento\Store\Model\ScopeInterface;

class Logo extends MagentoHeaderLogo
{
    /**
     * @param Context $context
     * @param Database $fileStorageHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Database $fileStorageHelper,
        array $data = []
    ) {
        parent::__construct($context, $fileStorageHelper, $data);
    }

    /**
     * Get sticky logo image URL
     *
     * @return string|false
     */
    public function getStickyLogoSrc()
    {
        return $this->getAdditionalLogoSrc('_sticky');
    }

    /**
     * TODO: finish, test and optimize it.
     * Optimize: no need to find out default logo path again. It's already done in parent class method _getLogoUrl(). Override that method.
     *
     * Get logo image URL with suffix
     *
     * @return string|false
     */
    protected function getAdditionalLogoSrc($suffix)
    {
        //Get default logo
        $storeLogoPath = $this->_scopeConfig->getValue(
            'design/header/logo_src',
            ScopeInterface::SCOPE_STORE
            //\Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $folderName = \Magento\Config\Model\Config\Backend\Image\Logo::UPLOAD_DIR;
        $path = $folderName . '/' . $storeLogoPath;

        if ($storeLogoPath !== null && $this->_isFile($path))
        {
            $defaultLogoSrc = $storeLogoPath;
        }
        elseif ($this->getLogoFile())
        {
            $defaultLogoSrc = $this->getLogoFile(); //Get param from XML
        }
        else
        {
            $defaultLogoSrc = 'images/logo.svg';
        }

        //Get new logo with suffix
        $info = pathinfo($defaultLogoSrc);
        $newLogoSrc = $info['dirname'] . '/' . $info['filename'] . $suffix . '.' . $info['extension'];
        $newPath = $folderName . '/' . $newLogoSrc;
        $newLogoUrl = $this->_urlBuilder
            ->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $newPath;

        //Check if image file exists
        if ($this->_isFile($newPath))
        {
            return $newLogoUrl;
        }
        else
        {
            return false;
        }
    }

}
