<?php

namespace Infortis\UltraSlideshow\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    /**
     * @var ScopeConfigInterface
     */
    protected $_configScopeConfigInterface;

    public function __construct(Context $context
//         ScopeConfigInterface $configScopeConfigInterface
    )
    {
        $this->_configScopeConfigInterface = $context->getScopeConfig();

        parent::__construct($context);
    }

	/**
	 * Get settings
	 *
	 * @return string
	 */
	public function getCfg($optionString)
    {
        return $this->_configScopeConfigInterface->getValue('ultraslideshow/' . $optionString);
    }

	/**
	 * Get slideshow position
	 *
	 * @return string
	 */
	/*
	public function getPosition()
    {
    	return $this->getCfg('general/position');
    }
    */
}
