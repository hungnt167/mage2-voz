<?php
/**
 * Copyright © 2015 Customer Paradigm. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace CustomerParadigm\OrderComments\Model\Checkout;

class PaymentInformationManagementPlugin
{

    /** @var \Magento\Sales\Model\Order\Status\HistoryFactory $historyFactory */
	protected $historyFactory;
	/** @var \Magento\Sales\Model\OrderFactory $orderFactory */
	protected $orderFactory;
	/** @var \Magento\Quote\Api\CartRepositoryInterface */
	protected $quoteRepository;

    /**
     * @param \Magento\Sales\Model\Order\Status\HistoryFactory $historyFactory
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     */
    public function __construct(
		\Magento\Sales\Model\Order\Status\HistoryFactory $historyFactory,
		\Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
    ) {
		$this->historyFactory = $historyFactory;
		$this->orderFactory = $orderFactory;
		$this->quoteRepository = $quoteRepository;
    }

    /**
     * @param \Magento\Checkout\Model\PaymentInformationManagement $subject
     * @param \Closure $proceed
	 * @param int $cartId
	 * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
	 * @param \Magento\Quote\Api\Data\AddressInterface $billingAddress
	 *
	 * @return int $orderId
     */
    public function aroundSavePaymentInformationAndPlaceOrder(
		\Magento\Checkout\Model\PaymentInformationManagement $subject, 
		\Closure $proceed,
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {	
		/** @param string $comment */
		$comment = NULL;
		// get JSON post data
		$request_body = file_get_contents('php://input');
		// decode JSON post data into array
		$data = json_decode($request_body, true);
		// get order comments
		if (isset ($data['comments'])) {
			// make sure there is a comment to save
			if ($data['comments']) {
				// remove any HTML tags
				$comment = strip_tags ($data['comments']);
				//$comment = 'ORDER COMMENT:  ' . $comment;
			}
		}
		// add comment to quote BEFORE success emails is sended
		if ($comment) {
		    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		    //$quote = $objectManager->create('Magento\Quote\Model\Quote')->loadActive($cartId);
		    $quote = $this->quoteRepository->getActive($cartId);
		    $quote->setCustomerNoteNotify(true);
		    $quote->setCustomerNote($comment);
		    $quote->save();
		}
		// run parent method and capture int $orderId
		$orderId = $proceed($cartId, $paymentMethod, $billingAddress);
		// if $comments
		// history already saved after $quote->save(); command
		if (0 && $comment) {
		    $comment = 'ORDER COMMENT:  ' . $comment;
			/** @param \Magento\Sales\Model\OrderFactory $order */
			$order = $this->orderFactory->create()->load($orderId);
			// make sure $order exists 
			if ($order->getData('entity_id')) {
				/** @param string $status */
				$status = $order->getData('status');
				/** @param \Magento\Sales\Model\Order\Status\HistoryFactory $history */
				$history = $this->historyFactory->create();
				// set comment history data
				$history->setData('comment', $comment);
				$history->setData('parent_id', $orderId);
				$history->setData('is_visible_on_front', 1);
				$history->setData('is_customer_notified', 0);
				$history->setData('entity_name', 'order');
				$history->setData('status', $status);
				$history->save();
			}
		}
		return $orderId;
    }
}