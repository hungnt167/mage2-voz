<?php
namespace Step\Three\Model\ResourceModel\Modelthree;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Step\Three\Model\Modelthree','Step\Three\Model\ResourceModel\Modelthree');
    }
}
