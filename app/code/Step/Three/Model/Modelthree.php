<?php
namespace Step\Three\Model;
class Modelthree extends \Magento\Framework\Model\AbstractModel implements \Step\Three\Api\Data\ModelthreeInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'step_three_modelthree';

    protected function _construct()
    {
        $this->_init('Step\Three\Model\ResourceModel\Modelthree');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
