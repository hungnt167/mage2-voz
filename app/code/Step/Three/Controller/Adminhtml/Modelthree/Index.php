<?php
namespace Step\Three\Controller\Adminhtml\Modelthree;

class Index extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Step_Three::write';  
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/index/index');
        return $resultRedirect;
    }     
}
