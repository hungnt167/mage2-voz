<?php
/**
 * Created by PhpStorm.
 * User: hungnt
 * Date: 5/11/17
 * Time: 5:24 PM
 */

namespace Step\Three\Controller\Adminhtml\Modelthree;


use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Step\Three\Model\ModelthreeFactory;
use Magento\Framework\Controller\ResultFactory;

class MassDelete extends Action
{
    protected $filter;
    protected $modelthreeFactory;

    public function __construct(Action\Context $context, Filter $filter, ModelthreeFactory $modelthreeFactory)
    {

        $this->modelthreeFactory = $modelthreeFactory;
        $this->filter            = $filter;

        parent::__construct($context);
    }


    public function execute()
    {
        $collection = $this->filter->getCollection($this->modelthreeFactory->create()->getCollection());
        $deleted = 0;
        foreach ($collection->getItems() as $record) {
            $record->delete();
            $deleted++;
        }
        $this->messageManager->addSuccess(
            __('A total of %1 record(s) have been deleted.', $deleted)
        );

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('step_three/*/index');
    }

}