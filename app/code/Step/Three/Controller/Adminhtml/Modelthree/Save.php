<?php
namespace Step\Three\Controller\Adminhtml\Modelthree;

use Magento\Backend\App\Action;
use Step\Three\Model\Page;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
            
class Save extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Step_Three::write';

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @param Action\Context $context
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Action\Context $context,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
//        var_dump($data);die;
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (isset($data['is_active']) && $data['is_active'] === 'true') {
                $data['is_active'] = Step\Three\Model\Modelthree::STATUS_ENABLED;
            }

            if (!empty($data['alert_days'])) {
                $data['alert_days'] = implode(',', $data['alert_days']);
            }

            if (!empty($data['completed_at'])) {
                $data['completed_at'] = new \DateTime($data['completed_at']);
            }

            if (empty($data['step_three_modelthree_id'])) {
                $data['step_three_modelthree_id'] = null;
            }

            /** @var Step\Three\Model\Modelthree $model */
            $model = $this->_objectManager->create('Step\Three\Model\Modelthree');

            $id = $this->getRequest()->getParam('step_three_modelthree_id');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccess(__('You saved the thing.'));
                $this->dataPersistor->clear('step_three_modelthree');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['step_three_modelthree_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }

            $this->dataPersistor->set('step_three_modelthree', $data);
            return $resultRedirect->setPath('*/*/edit', ['step_three_modelthree_id' => $this->getRequest()->getParam('step_three_modelthree_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }    
}
