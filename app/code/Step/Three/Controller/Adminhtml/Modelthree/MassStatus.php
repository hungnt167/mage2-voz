<?php
/**
 * Created by PhpStorm.
 * User: hungnt
 * Date: 5/11/17
 * Time: 5:24 PM
 */

namespace Step\Three\Controller\Adminhtml\Modelthree;


use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Step\Three\Model\Modelthree;
use Step\Three\Model\ModelthreeFactory;
use Magento\Framework\Controller\ResultFactory;

class MassStatus extends Action
{
    protected $filter;
    protected $modelthreeFactory;

    public function __construct(Action\Context $context, Filter $filter, ModelthreeFactory $modelthreeFactory)
    {

        $this->modelthreeFactory = $modelthreeFactory;
        $this->filter            = $filter;

        parent::__construct($context);
    }


    public function execute()
    {
        $collection = $this->filter->getCollection($this->modelthreeFactory->create()->getCollection());
        $count = 0;
        $status = $this->getRequest()->getParam('status');
        /** @var Modelthree $record */
        foreach ($collection->getItems() as $record) {
            $record->setStatus($status);
            $record->save();
            $count++;
        }
        $this->messageManager->addSuccess(
            __('A total of %1 record(s) have been updated.', $count)
        );

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('step_three/*/index');
    }

}