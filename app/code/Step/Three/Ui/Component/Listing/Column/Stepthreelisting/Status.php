<?php
namespace Step\Three\Ui\Component\Listing\Column\Stepthreelisting;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

class Status extends \Magento\Ui\Component\Listing\Columns\Column
{
    protected $statusMap;

    public function __construct(ContextInterface $context, UiComponentFactory $uiComponentFactory, array $components = [], array $data = [])
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);

        $this->statusMap = array(
            0 => __('Inactive'),
            1 => __('Active'),
        );
    }


    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                $name = $this->getData("name");

                $item[$name] = $this->statusMap[$item['is_active']];
            }
        }

        return $dataSource;
    }


}
