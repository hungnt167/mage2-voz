<?php
/**
 * Created by PhpStorm.
 * User: hungnt
 * Date: 5/10/17
 * Time: 5:41 PM
 */

namespace Step\Three\Ui\Component\Listing\Column\Stepthreelisting;


use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class AlertDays extends Column
{
    protected $dayMap;

    public function __construct(ContextInterface $context, UiComponentFactory $uiComponentFactory, array $components = [], array $data = [])
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);

        $this->dayMap = array(
            '0' => 'Mon',
            '1' => 'Tue',
            '2' => 'Wed',
            '3' => 'Thu',
            '4' => 'Fri',
            '5' => 'Sat',
            '6' => 'Sun',
        );
    }


    public function prepareDataSource(array $dataSource)
    {
        if (!empty($dataSource['data']['items'])) {
            $name = $this->getName();

            foreach ($dataSource['data']['items'] as $k => & $value) {

                $alertDays = '';
                $alertDayArr = explode(',', $value['alert_days']);


                if (  is_array($alertDayArr)) {
                    foreach ($alertDayArr as $dayNum) {
                        $alertDays .= $this->dayMap[$dayNum].',';
                    }

                    $alertDays = substr($alertDays, 0, -1);
                }

                $value[$name] = $alertDays;
            }

        }

        return $dataSource;
    }

}