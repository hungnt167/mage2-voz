<?xml version="1.0"?>
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:Acl/etc/acl.xsd">
    <acl>
        <resources>
            <resource id="Magento_Backend::admin">
                <resource id="Step_Three::top" title="TITLE HERE FOR">
                    <resource id="Step_Three::config" title="TITLE HERE FOR">
                        <resource id="Step_Three::read" title="TITLE HERE FOR"/>
                    </resource>
                </resource>
            </resource>
        </resources>
    </acl>
</config>
