<?php
namespace Step\Three\Api;

use Step\Three\Api\Data\ModelthreeInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface ModelthreeRepositoryInterface 
{
    public function save(ModelthreeInterface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(ModelthreeInterface $page);

    public function deleteById($id);
}
