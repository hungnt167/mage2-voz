/**
 * Created by hungnt on 4/26/17.
 */

define('One',['jquery'], function (jQuery) {
    return {
        changeColor: function (colorString) {
            let color = colorString;

            if (!color) {
                color = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
            }

            jQuery('body').css('background', color);

        }
    }
});