<?php
/**
 * Created by PhpStorm.
 * User: hungnt
 * Date: 4/27/17
 * Time: 5:34 PM
 */

namespace Step\One\Block;

use \Magento\Framework\View\Element\Template;
use \Step\One\Model\ModeloneFactory;

class View extends Template
{
    protected $modeloneFactory;

    public function __construct(Template\Context $context, array $data = [], ModeloneFactory $modeloneFactory)
    {
        $this->modeloneFactory = $modeloneFactory;
        parent::__construct($context, $data);
    }


    protected function _prepareLayout()
    {
        $this->setItems($this->modeloneFactory->create()->getCollection());
    }


}