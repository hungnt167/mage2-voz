<?php
/**
 * Created by PhpStorm.
 * User: hungnt
 * Date: 4/27/17
 * Time: 5:13 PM
 */

namespace Step\One\Controller\Action;

use \Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\View\Result\PageFactory;
use \Step\One\Model\ModeloneFactory;

class Save extends Action
{
    protected $pageFactory;
    protected $modeloneFactory;

    public function __construct(Context $context, PageFactory $pageFactory, ModeloneFactory $modeloneFactory)
    {
        $this->pageFactory = $pageFactory;
        $this->modeloneFactory = $modeloneFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        $requestData = $this->getRequest()->getParams();
        $record = $this->modeloneFactory->create();
        $record->setData($requestData);
        $record->save();

        $this->_redirect('*/*/view');

        //$this->pageFactory->create();
    }

}