<?php
/**
 * Created by PhpStorm.
 * User: hungnt
 * Date: 4/27/17
 * Time: 5:19 PM
 */

namespace Step\One\Controller\Action;

use \Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\View\Result\PageFactory;

class View extends Action
{
    protected $pageFactory;
    protected $modeloneFactory;

    public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }


    public function execute()
    {

        return $this->pageFactory->create();
    }

}