<?php

/**
 * Created by PhpStorm.
 * User: hungnt
 * Date: 4/15/17
 * Time: 8:38 PM
 */

namespace Step\One\Controller\Step;

use \Magento\Framework\App\Action\Action;
use \Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;

class One extends Action
{
    protected $pageFactory;

    public function __construct (Context $context, PageFactory $pageFactory)
    {
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute ()
    {
        // TODO: Implement execute() method.
        return $this->pageFactory->create();
    }


}