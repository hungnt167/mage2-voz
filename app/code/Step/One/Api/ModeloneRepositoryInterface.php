<?php
namespace Step\One\Api;

use Step\One\Api\Data\ModeloneInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface ModeloneRepositoryInterface 
{
    public function save(ModeloneInterface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(ModeloneInterface $page);

    public function deleteById($id);
}
