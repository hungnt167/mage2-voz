<?php
namespace Step\One\Model\ResourceModel\Modelone;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Step\One\Model\Modelone','Step\One\Model\ResourceModel\Modelone');
    }
}
