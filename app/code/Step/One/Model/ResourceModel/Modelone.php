<?php
namespace Step\One\Model\ResourceModel;
class Modelone extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('step_one_modelone','step_one_modelone_id');
    }
}
