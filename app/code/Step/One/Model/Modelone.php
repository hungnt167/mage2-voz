<?php
namespace Step\One\Model;
class Modelone extends \Magento\Framework\Model\AbstractModel implements \Step\One\Api\Data\ModeloneInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'step_one_modelone';

    protected function _construct()
    {
        $this->_init('Step\One\Model\ResourceModel\Modelone');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
