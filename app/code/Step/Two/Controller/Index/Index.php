<?php

namespace Step\Two\Controller\Index;
use Step\Two\Model\ModeltwoRepository;

class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;

        return parent::__construct($context);
    }

    public function execute()
    {
        /** @var ModeltwoRepository $repo */
        $repo = $this->_objectManager->get('Step\Model\ModeltwoRepository');

        \Zend_Debug::dump($repo->getList());die;

        return $this->resultPageFactory->create();
    }
}
