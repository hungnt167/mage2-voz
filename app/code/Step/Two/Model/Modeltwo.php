<?php
namespace Step\Two\Model;
class Modeltwo extends \Magento\Framework\Model\AbstractModel implements \Step\Two\Api\Data\ModeltwoInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'step_two_modeltwo';

    protected function _construct()
    {
        $this->_init('Step\Two\Model\ResourceModel\Modeltwo');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
