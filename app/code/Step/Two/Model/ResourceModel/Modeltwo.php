<?php
namespace Step\Two\Model\ResourceModel;
class Modeltwo extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('step_two_modeltwo','step_two_modeltwo_id');
    }
}
