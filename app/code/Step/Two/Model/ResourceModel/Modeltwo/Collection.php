<?php
namespace Step\Two\Model\ResourceModel\Modeltwo;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Step\Two\Model\Modeltwo','Step\Two\Model\ResourceModel\Modeltwo');
    }
}
