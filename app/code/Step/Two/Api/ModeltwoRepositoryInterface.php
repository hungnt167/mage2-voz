<?php
namespace Step\Two\Api;

use Step\Two\Api\Data\ModeltwoInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface ModeltwoRepositoryInterface 
{
    public function save(ModeltwoInterface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(ModeltwoInterface $page);

    public function deleteById($id);
}
