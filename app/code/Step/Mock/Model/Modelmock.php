<?php
namespace Step\Mock\Model;
class Modelmock extends \Magento\Framework\Model\AbstractModel implements \Step\Mock\Api\Data\ModelmockInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'step_mock_modelmock';

    protected function _construct()
    {
        $this->_init('Step\Mock\Model\ResourceModel\Modelmock');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
