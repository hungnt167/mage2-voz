<?php
namespace Step\Mock\Model\ResourceModel;
class Modelmock extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('step_mock_modelmock','step_mock_modelmock_id');
    }
}
