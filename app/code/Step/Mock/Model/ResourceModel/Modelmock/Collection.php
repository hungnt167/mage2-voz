<?php
namespace Step\Mock\Model\ResourceModel\Modelmock;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Step\Mock\Model\Modelmock','Step\Mock\Model\ResourceModel\Modelmock');
    }
}
