<?php
namespace Step\Mock\Api;

use Step\Mock\Api\Data\ModelmockInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface ModelmockRepositoryInterface 
{
    public function save(ModelmockInterface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(ModelmockInterface $page);

    public function deleteById($id);
}
