<?php
namespace Step\Mock\Controller\Adminhtml\Modelmock;

class Index extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Step_Mock::write';  
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/index/index');
        return $resultRedirect;
    }     
}
