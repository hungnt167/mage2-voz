<?php
namespace Step\Mock\Controller\Adminhtml\Modelmock;

class NewAction extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Step_Mock::write';       
    protected $resultPageFactory;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;        
        return parent::__construct($context);
    }
    
    public function execute()
    {
        return $this->resultPageFactory->create();  
    }    
}
