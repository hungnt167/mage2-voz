<?php
namespace Step\Mock\Controller\Adminhtml\Modelmock;

use Magento\Backend\App\Action;
use Step\Mock\Model\Page;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
            
class Save extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Step_Mock::write';

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @param Action\Context $context
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Action\Context $context,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (isset($data['is_active']) && $data['is_active'] === 'true') {
                $data['is_active'] = \Step\Mock\Model\Modelmock::STATUS_ENABLED;
            }
            if (empty($data['step_mock_modelmock_id'])) {
                $data['step_mock_modelmock_id'] = null;
            }

            /** @var Step\Mock\Model\Modelmock $model */
            $model = $this->_objectManager->create('Step\Mock\Model\Modelmock');

            $id = $this->getRequest()->getParam('step_mock_modelmock_id');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccess(__('You saved the thing.'));
                $this->dataPersistor->clear('step_mock_modelmock');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['step_mock_modelmock_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }

            $this->dataPersistor->set('step_mock_modelmock', $data);
            return $resultRedirect->setPath('*/*/edit', ['step_mock_modelmock_id' => $this->getRequest()->getParam('step_mock_modelmock_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }    
}
