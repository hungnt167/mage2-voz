<?php

namespace Step\Mock\Controller\Adminhtml\Index;

use Magento\Framework\Api\SearchCriteria;

class Index extends \Magento\Backend\App\Action
{

    const ADMIN_RESOURCE = 'ACL RULE HERE';

    protected $resultPageFactory;
    protected $injectionHelper;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Step\Mock\Model\Helper $helper
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->injectionHelper   = $helper;

        return parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Step\Mock\Model\ModelmockRepository $repo */
        $repo = $this->_objectManager->get('Step\Mock\Model\ModelmockRepository');

        /** @var SearchCriteria $searchCriteria */
        $searchCriteria = $this->_objectManager->get('Magento\Framework\Api\SearchCriteriaBuilder')
            ->addFilter('title', '%Bob%', 'like')
            ->create();

        $list = $repo->getList($searchCriteria);


        $page = $this->resultPageFactory->create();

        $page->setActiveMenu('Step_Mock::step_mock');

        return $page;
    }
}
