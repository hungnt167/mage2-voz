<?php
/**
 * Created by PhpStorm.
 * User: hungnt
 * Date: 5/8/17
 * Time: 6:04 PM
 */

namespace Step\Mock\Ui\Component\Listing\Column\Stepmocklistgrid;


use Magento\Ui\Component\Listing\Columns\Column;

class Status extends Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                $name = $this->getData("name");

                $item[$name] = $item[$name]?__("Active"):__('Inactive');
            }
        }

        return $dataSource;
    }

}