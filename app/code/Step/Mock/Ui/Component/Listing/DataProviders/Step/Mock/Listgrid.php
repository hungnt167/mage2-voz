<?php
namespace Step\Mock\Ui\Component\Listing\DataProviders\Step\Mock;

class Listgrid extends \Magento\Ui\DataProvider\AbstractDataProvider
{    
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Step\Mock\Model\ResourceModel\Modelmock\CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {

        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }
}
