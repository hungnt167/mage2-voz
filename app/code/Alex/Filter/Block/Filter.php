<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Shopby
 */

/**
 * Copyright © 2016 Amasty. All rights reserved.
 */

namespace Alex\Filter\Block;

use Magento\Framework\View\Element\Template;

class Filter extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Alex\Filter\Helper\Data
     */
    protected $helper;
    protected $_storeConfig;

    public function __construct(
        Template\Context $context,
        \Alex\Filter\Helper\Data $helper,
        array $data = []
    )
    {
        $this->helper = $helper;
        $this->_storeConfig = $context->getScopeConfig();

        parent::__construct($context, $data);
    }

    public function getHelper()
    {
        return $this->helper;
    }

    public function getPlaceholderImage(){

        return $this->getHelper()->getPlaceholderImg();
    }

}
