<?php
/**
 * Created by PhpStorm.
 * User: hungnt
 * Date: 5/12/17
 * Time: 4:00 PM
 */

namespace Alex\Filter\Model\Source;


use Magento\ConfigurableProduct\Model\AttributesList;
use Magento\Framework\Data\OptionSourceInterface;

class Attributes extends AttributesList implements OptionSourceInterface
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $collection = $this->collectionFactory->create();
        $attributes = [];
        foreach ($collection->getItems() as $attribute) {
            $attributes[] = [
                'id' => $attribute->getId(),
                'label' => $attribute->getFrontendLabel(),
                'value' => $attribute->getAttributeCode(),
            ];
        }

        return $attributes;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [0 => __('No'), 1 => __('Yes')];
    }
}