<?php
/**
 * Created by PhpStorm.
 * User: hungnt
 * Date: 5/13/17
 * Time: 2:58 PM
 */

namespace Alex\Filter\Controller\Filter;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Data extends \Magento\Framework\App\Action\Action
{
    protected $helper;

    public function __construct(Context $context, \Alex\Filter\Helper\Data $helper)
    {
        $this->helper = $helper;

        parent::__construct($context);
    }


    public function execute()
    {
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($this->helper->generateData());
        return $resultJson;
    }

}