<?php

/**
 * Created by PhpStorm.
 * User: hungnt
 * Date: 5/13/17
 * Time: 10:13 AM
 */

namespace Alex\Filter\Helper;

use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Layer;
use \Magento\Catalog\Model\Layer\Category\FilterableAttributeList;
use Magento\Catalog\Model\Layer\FilterList;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ObjectManager;
use \Magento\Framework\App\State;
use Magento\Catalog\Helper\Image;

class Data extends AbstractHelper
{
    const PREFIX = 'u_';

    protected $coreRegistry;
    protected $_storeManager;
    protected $filterConfig;
    protected $layer;

    /**
     * @var \Magento\Catalog\Model\Layer\FilterList
     */
    protected $filterList;

    /**
     * @var FilterableAttributeList
     */
    protected $filterableAttributes;

    protected $imageHelper;

    protected $showOOStock;

    /**
     * Data constructor.
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param FilterableAttributeList $filterableAttributes
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Registry $coreRegistry,
        FilterableAttributeList $filterableAttributes,
        Image $image
    )
    {
        parent::__construct($context);

        $this->imageHelper = $image;

        $this->coreRegistry = $coreRegistry;

        $this->scopeConfig = $context->getScopeConfig();

        $this->showOOStock = $this->scopeConfig->isSetFlag(
            'cataloginventory/options/show_out_of_stock',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $this->filterableAttributes = $filterableAttributes;

        $filterableAttributes = ObjectManager::getInstance()->get(FilterableAttributeList::class);

        /** @var State $appState */
        $appState      = ObjectManager::getInstance()->get(State::class);
        $layerResolver = ObjectManager::getInstance()->get(Resolver::class);

        $this->filterList = ObjectManager::getInstance()->create(
            FilterList::class,
            [
                'filterableAttributes' => $filterableAttributes,
                'filters'              => [
                    FilterList::CATEGORY_FILTER  => \Magento\Catalog\Model\Layer\Filter\Category::class,
                    FilterList::ATTRIBUTE_FILTER => \Amasty\Shopby\Model\Layer\Filter\Attribute::class,
                    FilterList::PRICE_FILTER     => \Magento\Catalog\Model\Layer\Filter\Price::class,
                    FilterList::DECIMAL_FILTER   => \Magento\Catalog\Model\Layer\Filter\Decimal::class,
                ],
            ]
        );

        if (!$appState->getAreaCode()) {
            $appState->setAreaCode('frontend');
        }

        /** @var Layer $layer */
        $this->layer = $layerResolver->get();

        $this->_storeManager = \Magento\Framework\App\ObjectManager::getInstance()->create('\Magento\Store\Model\StoreManager');
    }


    public function generateData()
    {
        $data         = array(
            'categories' => array(),
        );
        $rootCategory = $this->getCategoryLevel1();


        /** @var Category $categoryLv1 */
        foreach ($rootCategory as $categoryLv1) {
            $data['categories'][] = array(
                'id'   => $categoryLv1->getId(),
                'name' => $categoryLv1->getName(),
            );

            $subCategories = $this->getSubCategories($categoryLv1);

            if ($subCategories) {
                /** @var Category $categoryLv2 */
                foreach ($subCategories as $categoryLv2) {
                    $data['subCategories_' . $categoryLv1->getId()][] = array(
                        'id'   => $categoryLv2->getId(),
                        'name' => $categoryLv2->getName(),
                        //                        'image' => $categoryLv2->getImageUrl(),
                        'url'  => $categoryLv2->getUrl(),
                    );

                    if (!$categoryLv2->getProductCount()) {
                        continue;
                    }

                    $productCollection = $categoryLv2->getProductCollection();

//                    if (!$productCollection->isEnabledFlat()) {
                    $productCollection->joinTable('catalog_product_flat_' . $this->_storeManager->getStore()->getId(), 'entity_id = entity_id', array('*'));
//                    }

                    /** @var Product $firstProduct */
                    $firstProduct = $productCollection->getFirstItem();

                    $attributes = $firstProduct->getAttributes();

                    $data['productSampleData'] = $firstProduct->getData();

                    $lv45 = array();

                    foreach ($attributes as $attributeCode => $attribute) {
                        if (
                            (strpos($attributeCode, self::PREFIX) === 0) &&
                            $attributeCode != 'u_datasheet' &&
                            $attribute->getIsVisibleOnFront() &&
                            $attribute->getIsUserDefined()
                        ) {

                            $attrVal = $firstProduct->getData($attributeCode);

                            if ($attrVal) {
                                $lv45[] = $attribute;
                            }

//                            if (!$attribute->getData('is_filterable')) {
//                                $attribute->setData('is_filterable', 1);
//                                $attribute->save();
//                            }
//
//                            if (!$attribute->getData('visible_on_front')) {
//                                $attribute->setData('visible_on_front', 1);
//                                $attribute->save();
//                            }
//
                            if (!$attribute->getData('used_in_product_listing')) {
                                $attribute->setData('used_in_product_listing', 1);
                                $attribute->save();
                            }
                        }
                    }


                    $lv4 = array_shift($lv45);
                    $lv5 = array_shift($lv45);


                    $brands = $this->getBrandsByCategory($categoryLv2, $productCollection, $lv4, $lv5);

                    if (!empty($brands)) {
                        foreach ($brands as $brandKey => $brand) {
                            $brandId   = $brand['code'];
                            $brandData = array(
                                'id'    => $brandId,
                                'name'  => $brand['label'],
                                'image' => $brand['image'],
                                'url'   => $brand['url'],
                            );

                            if (empty($data['brands_' . $categoryLv2->getId()])) {
                                $data['brands_' . $categoryLv2->getId()] = array($brandData);
                            } else {
                                $data['brands_' . $categoryLv2->getId()][] = $brandData;
                            }

                            $level4s = $brand['children'];

                            if (!empty($level4s)) {
                                foreach ($level4s as $level4Key => $level4) {
                                    $level4Id   = $level4['code'];
                                    $level4Data = array(
                                        'id'    => $level4Id,
                                        'name'  => $level4['label'],
                                        'image' => $brand['image'],
                                        'url'   => $level4['url'],
                                    );

                                    if (empty($data['level4s_b' . $brandId . 'c' . $categoryLv2->getId()])) {
                                        $data['level4s_b' . $brandId . 'c' . $categoryLv2->getId()] = array($level4Data);
                                    } else {
                                        $data['level4s_b' . $brandId . 'c' . $categoryLv2->getId()][] = $level4Data;
                                    }

                                    $level5s = $level4['children'];

                                    if (!empty($level5s)) {
                                        foreach ($level5s as $level5Key => $level5) {
                                            $level5Id   = $level5['code'];
                                            $level5Data = array(
                                                'id'   => $level5Id,
                                                'name' => $level5['label'],
                                                'url'  => $level5['url'],
                                            );

                                            if (empty($data['level5s_' . $level4Id . 'b' . $brandId . 'c' . $categoryLv2->getId()])) {
                                                $data['level5s_' . $level4Id . 'b' . $brandId . 'c' . $categoryLv2->getId()] = array($level5Data);
                                            } else {
                                                $data['level5s_' . $level4Id . 'b' . $brandId . 'c' . $categoryLv2->getId()][] = $level5Data;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $data;
    }

    public function getRootCategory()
    {
        if ($category = $this->coreRegistry->registry('root_category')) {
            return $category;
        }


        $categoryId = $this->_storeManager->getStore()->getRootCategoryId();
        if (!$categoryId) {
            return false;
        }

        try {
            /** @var CategoryFactory $categoryFactory */
            $categoryFactory = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Catalog\Model\CategoryFactory');
            /** @var Category $category */
            $category = $categoryFactory->create();
            $category->setStoreId($this->_storeManager->getStore()->getId());
            $this->coreRegistry->register('root_category', $category->load($categoryId));

            return $category = $this->coreRegistry->registry('root_category');
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return false;
        }
    }

    public function getSubCategories($rootCategory)
    {
        if ($rootCategory) {
            $collection = $rootCategory->getCollection();
            $collection->addAttributeToSelect('url_key')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('all_children')
                ->addAttributeToSelect('is_anchor')
                ->addAttributeToSelect('image')
                ->addAttributeToFilter('is_active', 1)
                ->setOrder('entity_id', \Magento\Framework\DB\Select::SQL_DESC)
                ->addFieldToFilter('parent_id', $rootCategory->getId());

            $collection->load();
            if ($collection->count()) {
                return $collection;
            }
        }

        return null;
    }

    /**
     * @param \Magento\Catalog\Model\Category $rootCategory
     * @param $productCollection
     * @param Attribute|bool $lv4
     * @param Attribute|bool $lv5
     * @return array|null
     */
    public function getBrandsByCategory($rootCategory, $productCollection, $lv4 = false, $lv5 = false)
    {

        $attributeCodeLvl4 = $lv4 ? $lv4->getAttributeCode() : false;
        $attributeCodeLvl5 = $lv5 ? $lv5->getAttributeCode() : false;

        if ($productCollection->count()) {
            $brands = [];
            /** @var Product $product */
            foreach ($productCollection as $product) {
                if (
                    ($manufacturer = $product->getManufacturer()) &&
                    ($manufacturerLabel = $product->getManufacturerValue())
                    &&
                    ( $product->isSalable() || $this->showOOStock)
                ) {
                    $manufacturerKey = $manufacturer . 'c' . $rootCategory->getId();
                    if (!isset($brands [$manufacturerKey])) {
                        $brands [$manufacturerKey]                   = ['children' => []];
                        $brands [$manufacturerKey]['code']           = $manufacturer;
                        $brands [$manufacturerKey]['image']          = $this->getImageOfAttribute('manufacturer', $manufacturer, $rootCategory->getStoreId());
                        $brands [$manufacturerKey]['label']          = $manufacturerLabel;
                        $brands [$manufacturerKey]['attribute_code'] = 'manufacturer';
                        $brands [$manufacturerKey]['url']            = $rootCategory->getUrl() . '?manufacturer=' . $manufacturer;
                    }

                    if (
                        ($level4Value = $product->getData($attributeCodeLvl4)) &&
                        $lv4
                    ) {
                        $level4ValueKey = $level4Value . 'b' . $manufacturer . 'c' . $rootCategory->getId();
                        if ($lv4->getFrontendInput() == 'multiselect') {
                            foreach (explode(',', $level4Value) as $level4ValueSmall) {
                                $level4Label    = $lv4->getSource()->getOptionText($level4ValueSmall);
                                $level4ValueKey = $level4ValueSmall . 'b' . $manufacturer . 'c' . $rootCategory->getId();
                                if (!isset($brands[$manufacturerKey]['children'][$level4ValueKey])) {

                                    $brands [$manufacturerKey]['children'] [$level4ValueKey]                  = [];
                                    $brands [$manufacturerKey]['children'][$level4ValueKey]['children']       = [];
                                    $brands [$manufacturerKey]['children'][$level4ValueKey]['code']           = $level4ValueSmall;//shoudld be label
                                    $brands [$manufacturerKey]['children'][$level4ValueKey]['image']          = $this->getImageOfAttribute($attributeCodeLvl4, $level4ValueSmall, $rootCategory->getStoreId());
                                    $brands [$manufacturerKey]['children'][$level4ValueKey]['label']          = $level4Label;//shoudld be label
                                    $brands [$manufacturerKey]['children'][$level4ValueKey]['attribute_code'] = $attributeCodeLvl4;
                                    $brands [$manufacturerKey]['children'][$level4ValueKey]['url']
                                                                                                              = $brands [$manufacturerKey]['url'] . '&' . $attributeCodeLvl4 . '=' . $level4Value;
                                }
                            }
                        } else {
                            $level4Label = $product->getData($attributeCodeLvl4 . '_value');
                            if (!isset($brands[$manufacturerKey]['children'][$level4ValueKey])) {

                                $brands [$manufacturerKey]['children'] [$level4ValueKey]                  = [];
                                $brands [$manufacturerKey]['children'][$level4ValueKey]['children']       = [];
                                $brands [$manufacturerKey]['children'][$level4ValueKey]['code']           = $level4Value;//shoudld be label
                                $brands [$manufacturerKey]['children'][$level4ValueKey]['image']          = $this->getImageOfAttribute($attributeCodeLvl4, $level4Value, $rootCategory->getStoreId());
                                $brands [$manufacturerKey]['children'][$level4ValueKey]['label']          = $level4Label;//shoudld be label
                                $brands [$manufacturerKey]['children'][$level4ValueKey]['attribute_code'] = $attributeCodeLvl4;
                                $brands [$manufacturerKey]['children'][$level4ValueKey]['url']
                                                                                                          = $brands [$manufacturerKey]['url'] . '&' . $attributeCodeLvl4 . '=' . $level4Value;
                            }
                        }


                        if (
                            ($level5Value = $product->getData($attributeCodeLvl5)) &&
                            $lv5
                        ) {
                            $level5ValueKey = $level5Value . '4' . $level4Value . 'b' . $manufacturer . 'c' . $rootCategory->getId();
                            if ($lv5->getFrontendInput() == 'multiselect') {
                                foreach (explode(',', $level5Value) as $level5ValueSmall) {
                                    $level5Label    = $lv5->getSource()->getOptionText($level5ValueSmall);
                                    $level5ValueKey = $level5ValueSmall . '4' . $level4Value . 'b' . $manufacturer . 'c' . $rootCategory->getId();
                                    if (!isset($brands[$manufacturerKey]['children'][$level4ValueKey]['children'][$level5ValueKey])) {
                                        $brands[$manufacturerKey]['children'][$level4ValueKey]['children'][$level5ValueKey]                   = [];
                                        $brands[$manufacturerKey]['children'][$level4ValueKey]['children'][$level5ValueKey]['children']       = [];
                                        $brands[$manufacturerKey]['children'][$level4ValueKey]['children'][$level5ValueKey]['code']           = $level5ValueSmall;//shoudld be label
                                        $brands[$manufacturerKey]['children'][$level4ValueKey]['children'][$level5ValueKey]['label']          = $level5Label;//shoudld be label
                                        $brands[$manufacturerKey]['children'][$level4ValueKey]['children'][$level5ValueKey]['attribute_code'] = $attributeCodeLvl5;
                                        $brands[$manufacturerKey]['children'][$level4ValueKey]['children'][$level5ValueKey]['url']
                                                                                                                                              = $brands [$manufacturerKey]['children'][$level4ValueKey]['url'] . '&' . $attributeCodeLvl5 . '=' . $level5Value;
                                    }
                                }
                            } else {
                                $level5Label = $product->getData($attributeCodeLvl5 . '_value');
                                if (!isset($brands[$manufacturerKey]['children'][$level4ValueKey]['children'][$level5ValueKey])) {
                                    $brands[$manufacturerKey]['children'][$level4ValueKey]['children'][$level5ValueKey]                   = [];
                                    $brands[$manufacturerKey]['children'][$level4ValueKey]['children'][$level5ValueKey]['children']       = [];
                                    $brands[$manufacturerKey]['children'][$level4ValueKey]['children'][$level5ValueKey]['code']           = $level5Value;//shoudld be label
                                    $brands[$manufacturerKey]['children'][$level4ValueKey]['children'][$level5ValueKey]['label']          = $level5Label;//shoudld be label
                                    $brands[$manufacturerKey]['children'][$level4ValueKey]['children'][$level5ValueKey]['attribute_code'] = $attributeCodeLvl5;
                                    $brands[$manufacturerKey]['children'][$level4ValueKey]['children'][$level5ValueKey]['url']
                                                                                                                                          = $brands [$manufacturerKey]['children'][$level4ValueKey]['url'] . '&' . $attributeCodeLvl5 . '=' . $level5Value;
                                }
                            }

                        }
                    }
                }

            }

            return $brands;
        }
    }


    /**
     * @return array
     */
    public function getCategoryLevel1()
    {
        /** @var \Magento\Catalog\Model\Category $rootCategory */
        $rootCategory = $this->getRootCategory();

        return $this->getSubCategories($rootCategory);
    }

    /**
     * @return string
     */
    public function getDataUrl()
    {
        return $this->_getUrl('alexfilter/filter/data');
    }

    public function getPlaceholderImg()
    {
        return $this->imageHelper->getDefaultPlaceholderUrl('thumbnail');
    }

    /**
     * @param string $attributeCode
     * @param string $optionId
     * @param string $storeId
     * @return string
     */
    public function getImageOfAttribute($attributeCode, $optionId, $storeId)
    {
        /** @var \Amasty\Shopby\Model\OptionSetting $model */
        $model = ObjectManager::getInstance()->create('Amasty\Shopby\Model\OptionSetting');

        $model = $model->getByParams('attr_'.$attributeCode, $optionId, $storeId);

        if (!$model->getImageUrl()) {
            return $this->getPlaceholderImg();
        }

        return $model->getImageUrl();
    }
}