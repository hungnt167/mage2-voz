<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/2/2017
 * Time: 10:38 AM
 */

namespace Wazza\Pdf\Helper;

use Wazza\Pdf\Helper\Data as Helper;
use Wazza\Pdf\Helper\IntegrateInterface as IntegrateInterface;

abstract class IntegrateAbstract extends Helper implements IntegrateInterface
{
	protected $object;
	protected $model;
	protected $collection;

	public function setModel($model) {
		$this->model = $model;
		return $this;
	}
    public function setCollection($collection) {
    	$this->collection = $collection;
    	return $this;
    }
    public function setObject($object) {
    	$this->object = $object;
    	return $this;
    }

    abstract public function execute();

}