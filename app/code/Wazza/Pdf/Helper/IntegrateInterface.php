<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/2/2017
 * Time: 10:38 AM
 */

namespace Wazza\Pdf\Helper;

interface IntegrateInterface
{
    public function setModel($model);
    public function setCollection($collection);
    public function setObject($object);
}