<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/2/2017
 * Time: 10:38 AM
 */

namespace Wazza\Pdf\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
   protected $resource;
        
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,        
        \Magento\Framework\App\ResourceConnection $resource
    )
    {    
        $this->resource = $resource;
        parent::__construct($context);
    }
    
    /**
     * Whether a module is enabled in the configuration or not
     *
     * @param string $moduleName Fully-qualified module name
     * @return boolean
     */
    public function isModuleEnabled($moduleName)
    {
        return $this->_moduleManager->isEnabled($moduleName);
    }
    
    /**
     * Whether a module output is permitted by the configuration or not
     *
     * @param string $moduleName Fully-qualified module name
     * @return boolean
     */
    public function isOutputEnabled($moduleName)
    {
        return $this->_moduleManager->isOutputEnabled($moduleName);
    }
}