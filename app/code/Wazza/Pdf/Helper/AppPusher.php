<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/2/2017
 * Time: 10:38 AM
 */

namespace Wazza\Pdf\Helper;

use Pusher\Pusher as Pusher;

class AppPusher
{
	const APP_CHANNEL = 'AppChannel';
	private static $socket;

	public static function getSocket()
	{
		if (!self::$socket) {
			$options = array(
				'cluster' => 'ap1',
				'encrypted' => true
			);
			self::$socket = new Pusher(
				'c7d3ff65e7b987e8d89d',
				'4bad401e38fb6ede5697',
				'444027',
				$options
			);
		}

		return self::$socket;
	}


	public static function trigger($eventName, $data)
	{
		self::getSocket()->trigger(self::APP_CHANNEL, $eventName, $data);
	}

}