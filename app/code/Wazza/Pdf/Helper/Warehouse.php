<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/2/2017
 * Time: 10:38 AM
 */

namespace Wazza\Pdf\Helper;

use Wazza\Pdf\Helper\IntegrateAbstract as IntegrateAbstract;

class Warehouse extends IntegrateAbstract
{

    public function execute() {
    	if ($this->model) {
    		
    		$this->model = null;
    	}

    	if ($this->collection) {
    		$this->addStockFilterToCollection($this->collection);
    		$this->collection = null;
    	}
    	
    }


    /**
     * Adds filtering for collection to return only in stock products
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection $collection
     * @return void
     */
    public function addStockFilterToCollection($collection)
    {
    	/*

string(1) "1"
  ["stock_id"]=>
  string(1) "1"
  ["qty"]=>
  NULL
  ["min_qty"]=>
  string(6) "0.0000"
  ["use_config_min_qty"]=>
  string(1) "1"
  ["is_qty_decimal"]=>
  string(1) "0"
  ["backorders"]=>
  string(1) "0"
  ["use_config_backorders"]=>
  string(1) "1"
  ["min_sale_qty"]=>
  string(6) "1.0000"
  ["use_config_min_sale_qty"]=>
  string(1) "1"
  ["max_sale_qty"]=>
  string(6) "0.0000"
  ["use_config_max_sale_qty"]=>
  string(1) "1"
  ["is_in_stock"]=>
  string(1) "1"
  ["low_stock_date"]=>
  NULL
  ["notify_stock_qty"]=>
  NULL
  ["use_config_notify_stock_qty"]=>
  string(1) "1"
  ["manage_stock"]=>
  string(1) "0"
  ["use_config_manage_stock"]=>
  string(1) "1"
  ["stock_status_changed_auto"]=>
  string(1) "0"
  ["use_config_qty_increments"]=>
  string(1) "1"
  ["qty_increments"]=>
  string(6) "0.0000"
  ["use_config_enable_qty_inc"]=>
  string(1) "1"
  ["enable_qty_increments"]=>
  string(1) "0"
  ["is_decimal_divided"]=>
  string(1) "0"
  ["website_id"]=>
  string(1) "0"
    	*/

  		$websiteId = 1;
  		if ($this->isModuleEnabled('Magestore_InventorySuccess')) {
	  		$websiteId = 2;
  		}

        $collection->getSelect()->join(
        		array('stock_item_table' => $this->resource->getTableName('cataloginventory_stock_item')),
        		"stock_item_table.product_id = e.entity_id and stock_item_table.website_id = {$websiteId}",
        		array(
        			'stock_id' => 'stock_id',
        			'qty',
        			'backorders',
        			'is_in_stock',
        			'manage_stock' => 'manage_stock',
        		)
        );
    }


}