<?php
namespace Wazza\Pdf\Model;
class WPTemplate extends \Magento\Framework\Model\AbstractModel implements \Wazza\Pdf\Api\Data\WPTemplateInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'wazza_pdf_wptemplate';

    protected function _construct()
    {
        $this->_init('Wazza\Pdf\Model\ResourceModel\WPTemplate');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
