<?php
namespace Wazza\Pdf\Model\ResourceModel;
class WPTemplate extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('wazza_pdf_wptemplate','wazza_pdf_wptemplate_id');
    }
}
