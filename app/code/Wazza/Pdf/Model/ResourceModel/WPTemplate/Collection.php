<?php
namespace Wazza\Pdf\Model\ResourceModel\WPTemplate;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Wazza\Pdf\Model\WPTemplate','Wazza\Pdf\Model\ResourceModel\WPTemplate');
    }
}
