<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/2/2017
 * Time: 10:38 AM
 */

namespace Wazza\Pdf\Plugin;

use Wazza\Pdf\Helper\AppPusher as AppPusher; 


class ProductPlugin
{
	public function afterSave(\Magento\Catalog\Model\Product $subject, $result)
	{
		if ($result) {
			$data = $subject->getData();
			AppPusher::trigger(
				$this->getEventName(__METHOD__) , 
				$data
			);
		}
		return $result;
	}

	public function getEventName($method)
	{
		return strtolower($this->getName()) . "-" . strtolower($this->getMethodName($method));
	}

	public function getName() {
		$path = explode('\\', __CLASS__);
		return array_pop($path);
	}

	public function getMethodName($full) {
		$path = explode('::', $full);
		return array_pop($path);
	}

}