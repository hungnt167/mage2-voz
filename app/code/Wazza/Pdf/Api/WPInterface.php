<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/2/2017
 * Time: 10:37 AM
 */

namespace Wazza\Pdf\Api;


interface WPInterface
{
    /**
     * @return mixed
     */
    public function check();
}