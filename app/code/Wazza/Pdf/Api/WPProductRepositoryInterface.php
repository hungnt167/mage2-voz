<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 8/2/2017
 * Time: 11:06 AM
 */

namespace Wazza\Pdf\Api;


use Magento\Catalog\Api\ProductRepositoryInterface;

interface WPProductRepositoryInterface extends ProductRepositoryInterface
{

}