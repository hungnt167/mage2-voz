<?php
namespace Wazza\Pdf\Api;

use Wazza\Pdf\Api\Data\WPTemplateInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface WPTemplateRepositoryInterface 
{
    public function save(WPTemplateInterface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(WPTemplateInterface $page);

    public function deleteById($id);
}
